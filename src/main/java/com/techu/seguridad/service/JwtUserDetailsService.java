package com.techu.seguridad.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        if ("techuniver".equals(s)) {
            return new User("techuniver", "$2y$12$u6vPaCt2cO4ySNRcw2wxl.2pZ7xA..cG0swv4E0IPpvCRbglsLB1C", new ArrayList<>());
        }
        throw new UsernameNotFoundException(String.format("Usuario %s no encontrado", s));
    }
}
